package br.com.lpdemilis.deliverycontrolapp.app;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.*;
import android.webkit.WebView;
import android.widget.*;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity implements LocationListener, OnMapReadyCallback {

    public static final int REQUEST_ACCESS_FINE_LOCATION = 1;
    public static final int REQUEST_READ_PHONE_STATE = 2;
    public static final Long DELAY_INTERVAL = Long.valueOf(60000);

    public static final int ACTION_UPDATE_CARRIER = 1;
    public static final int ACTION_GET_MY_DISPATCHS = 2;
    public static final int ACTION_UPDATE_DISPATCH = 3;

//    public static final String updateCarrierURL = "http://192.168.1.5/delivery-control/carriers/updateCarrier.json";
//    public static final String getMyDispatchsURL = "http://192.168.1.5/delivery-control/carriers/getMyDispatchs.json";
//    public static final String updateDispatchURL = "http://192.168.1.5/delivery-control/dispatchs/updateDispatch.json";
    public static final String updateCarrierURL = "http://lpdemilis.com/delivery-control/carriers/updateCarrier.json";
    public static final String getMyDispatchsURL = "http://lpdemilis.com/delivery-control/carriers/getMyDispatchs.json";
    public static final String updateDispatchURL = "http://lpdemilis.com/delivery-control/dispatchs/updateDispatch.json";

    public static final String status = "Online";

    private Boolean permissionCheck = false;
    private Boolean canceled;

    protected LocationManager locationManager;
    protected Location location;
    private CountDownTimer countDownTimer;

    private ProgressDialog progress;
    private TextView mainTextView;
    private WebView mainWebView;
    private TextView deviceIdTextView;
    private TextView timerTextView;
    private Switch onlineStatusSwitch;

    private TextView noPendingDispatchsTextView;
    private TextView noDeliveredDispatchsTextView;

    private TableLayout myPendingDispatchsTableLayout;
    private TableLayout myDeliveredDispatchsTableLayout;

    private LinearLayout carriersGroupLinearLayout;
    private TextView carriersInfoTextView;
    private LinearLayout carriersLinearLayout;

    private LinearLayout mainTextLinearLayout;

    private String position;
    private String deviceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainTextView = (TextView) findViewById(R.id.mainTextView);
        mainWebView = (WebView) findViewById(R.id.mainWebView);
        deviceIdTextView = (TextView) findViewById(R.id.deviceIdTextView);
        onlineStatusSwitch = (Switch) findViewById(R.id.onlineStatusSwitch);
        timerTextView = (TextView) findViewById(R.id.timerTextView);
        myPendingDispatchsTableLayout = (TableLayout) findViewById(R.id.myPendingDispatchsTableLayout);
        myDeliveredDispatchsTableLayout = (TableLayout) findViewById(R.id.myDeliveredDispatchsTableLayout);
        noPendingDispatchsTextView = (TextView) findViewById(R.id.noPendingDispatchsTextView);
        noDeliveredDispatchsTextView = (TextView) findViewById(R.id.noDeliveredDispatchsTextView);
        carriersLinearLayout = (LinearLayout) findViewById(R.id.carriersLinearLayout);
        carriersInfoTextView = (TextView) findViewById(R.id.carriersInfoTextView);
        carriersGroupLinearLayout = (LinearLayout) findViewById(R.id.carriersGroupLinearLayout);
        mainTextLinearLayout = (LinearLayout) findViewById(R.id.mainTextLinearLayout);

        progress = new ProgressDialog(MainActivity.this);

        if (!permissionCheck)
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);

        onlineStatusSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if (isChecked) {
                    canceled = false;
                    executeUpdate();
                    onlineStatusSwitch.setText("Send Status (Online)");
                    timerTextView.setVisibility(View.VISIBLE);
                    timerTextView.setText("Updating...");
                    carriersLinearLayout.setVisibility(View.VISIBLE);
                    carriersInfoTextView.setVisibility(View.VISIBLE);
                    carriersGroupLinearLayout.setVisibility(View.VISIBLE);
                } else {
                    if (countDownTimer != null)
                        countDownTimer.cancel();

                    if (canceled) {
                        timerTextView.setVisibility(View.VISIBLE);
                        timerTextView.setText("Canceled by the user...");
                    } else {
                        timerTextView.setText("");
                        timerTextView.setVisibility(View.GONE);
                    }

                    mainTextView.setText("");
                    mainTextView.setVisibility(View.GONE);
                    mainTextLinearLayout.setVisibility(View.GONE);
                    mainWebView.loadData("","text/html", "UTF-8");
                    mainWebView.setVisibility(View.GONE);
                    carriersLinearLayout.setVisibility(View.GONE);
                    carriersInfoTextView.setVisibility(View.GONE);
                    carriersGroupLinearLayout.setVisibility(View.GONE);
                    onlineStatusSwitch.setText("Send Status (Offline)");
                }
            }
        });

        progress.setCanceledOnTouchOutside(false);
        progress.setOnCancelListener(new ProgressDialog.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                canceled = true;
                onlineStatusSwitch.setChecked(false);
            }
        });
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Exit App")
                .setMessage("Are you sure you want to exit this app?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    permissionCheck = true;
                    onlineStatusSwitch.setEnabled(true);
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    permissionCheck = false;
                    onlineStatusSwitch.setEnabled(false);
                    onlineStatusSwitch.setChecked(false);
                    mainTextView.setText("ACCESS_FINE_LOCATION Permission Required");
                    mainTextView.setVisibility(View.VISIBLE);
                    mainWebView.loadData("","text/html", "UTF-8");
                    mainWebView.setVisibility(View.GONE);
                    mainTextLinearLayout.setVisibility(View.VISIBLE);
                }
                return;
            }
            case REQUEST_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    deviceId = telephonyManager.getDeviceId();
                    deviceIdTextView.setText(deviceId);

                    if (!permissionCheck)
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_ACCESS_FINE_LOCATION);

                    MyConnection myConnection = new MyConnection(MainActivity.this);
                    myConnection.action = String.valueOf(ACTION_GET_MY_DISPATCHS);
                    myConnection.requestURL = getMyDispatchsURL + "?imei=" + deviceId;
                    myConnection.execute();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    onlineStatusSwitch.setEnabled(false);
                    onlineStatusSwitch.setChecked(false);
                    deviceIdTextView.setText("Not Found");
                    mainTextView.setText("READ_PHONE_STATE Permission Required");
                    mainTextView.setVisibility(View.VISIBLE);
                    mainWebView.loadData("","text/html", "UTF-8");
                    mainWebView.setVisibility(View.GONE);
                    mainTextLinearLayout.setVisibility(View.VISIBLE);
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLocationChanged(Location location) {
        position = "{lat%3A+" + location.getLatitude() + "%2C+lng%3A+" + location.getLongitude() + "}";
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("Latitude", "disable");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("Latitude", "enable");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d("Latitude", "status");
    }

    public void executeUpdate() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, MainActivity.this);
        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        position = "{lat%3A+" + location.getLatitude() + "%2C+lng%3A+" + location.getLongitude() + "}";

        MyConnection myConnection = new MyConnection(MainActivity.this);
        myConnection.action = String.valueOf(ACTION_UPDATE_CARRIER);
        myConnection.requestURL = updateCarrierURL + "?imei=" + deviceId + "&status=" + status + "&position=" + position;
        myConnection.execute();
    }

    public void preExecute(String action) {
        switch (Integer.valueOf(action)) {
            case ACTION_UPDATE_CARRIER: {
                mainTextView.setText("");
                mainTextView.setVisibility(View.GONE);
                mainWebView.loadData("","text/html", "UTF-8");
                mainWebView.setVisibility(View.GONE);
                mainTextLinearLayout.setVisibility(View.GONE);
                progress.setMessage("Updating carrier");
                progress.show();
                return;
            }
            case ACTION_GET_MY_DISPATCHS: {
                progress.setMessage("Updating dispatchs");
                progress.show();
                return;
            }
        }
    }

    public void postExecute(String response, String action, String message) {
        switch (Integer.valueOf(action)) {
            case ACTION_UPDATE_CARRIER: {
                updateCarrierReturn(response, message);
                return;
            }
            case ACTION_GET_MY_DISPATCHS: {
                getMyDispatchsReturn(response, message);
                return;
            }
            case ACTION_UPDATE_DISPATCH: {
                MyConnection myConnection = new MyConnection(MainActivity.this);
                myConnection.action = String.valueOf(ACTION_GET_MY_DISPATCHS);
                myConnection.requestURL = getMyDispatchsURL + "?imei=" + deviceId;
                myConnection.execute();
                return;
            }
        }
    }

    public void updateCarrierReturn(String response, String message) {
        if (!onlineStatusSwitch.isChecked()) {
            return;
        }

        try {
            JSONObject responseJsonObject = new JSONObject(response);
            if (responseJsonObject.getString("status").equals("OK")) {
                JSONArray carriersJsonArray = responseJsonObject.getJSONArray("carriers");

                carriersLinearLayout.removeAllViews();

                int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PT, 3, this.getResources().getDisplayMetrics());
                int paddingBottom = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, this.getResources().getDisplayMetrics());

                for (int i = 0; i < carriersJsonArray.length(); i++) {
                    LinearLayout carrierInfoLinearLayout = new LinearLayout(this);
                    carrierInfoLinearLayout.setOrientation(LinearLayout.VERTICAL);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        carrierInfoLinearLayout.setBackground( getResources().getDrawable(R.drawable.layout_table_bg));
                    } else {
                        carrierInfoLinearLayout.setBackgroundDrawable( getResources().getDrawable(R.drawable.layout_table_bg) );
                    }

//                    LinearLayout carrierIdLinearLayout = new LinearLayout(this);
//                    carrierIdLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//                    TextView carrierIdLabelTextView = new TextView(this);
//                    carrierIdLabelTextView.setBackgroundColor(Color.parseColor("#EEEEEE"));
//                    carrierIdLabelTextView.setText("Carrier ID");
//                    carrierIdLabelTextView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1f));
//                    carrierIdLabelTextView.setPadding(padding,padding,padding,padding);
//                    TextView carrierIdValueTextView = new TextView(this);
//                    carrierIdValueTextView.setBackgroundColor(Color.parseColor("#EEEEEE"));
//                    carrierIdValueTextView.setText(carriersJsonArray.getJSONObject(i).getString("id"));
//                    carrierIdValueTextView.setGravity(Gravity.RIGHT);
//                    carrierIdValueTextView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1f));
//                    carrierIdValueTextView.setTypeface(Typeface.DEFAULT_BOLD);
//                    carrierIdValueTextView.setPadding(padding,padding,padding,padding);
//                    carrierIdLinearLayout.addView(carrierIdLabelTextView);
//                    carrierIdLinearLayout.addView(carrierIdValueTextView);
//                    carriersLinearLayout.addView(carrierIdLinearLayout);

                    LinearLayout carrierNameLinearLayout = new LinearLayout(this);
                    carrierNameLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    TextView carrierNameLabelTextView = new TextView(this);
                    carrierNameLabelTextView.setBackgroundColor(Color.parseColor("#EEEEEE"));
                    carrierNameLabelTextView.setText("Carrier Name");
                    carrierNameLabelTextView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1f));
                    carrierNameLabelTextView.setPadding(padding,padding,padding,padding);
                    TextView carrierNameValueTextView = new TextView(this);
                    carrierNameValueTextView.setBackgroundColor(Color.parseColor("#EEEEEE"));
                    carrierNameValueTextView.setText(carriersJsonArray.getJSONObject(i).getString("name"));
                    carrierNameValueTextView.setGravity(Gravity.RIGHT);
                    carrierNameValueTextView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1f));
                    carrierNameValueTextView.setTypeface(Typeface.DEFAULT_BOLD);
                    carrierNameValueTextView.setPadding(padding,padding,padding,padding);
                    carrierNameLinearLayout.addView(carrierNameLabelTextView);
                    carrierNameLinearLayout.addView(carrierNameValueTextView);
                    carrierInfoLinearLayout.addView(carrierNameLinearLayout);

//                    LinearLayout carrierBasePositionLinearLayout = new LinearLayout(this);
//                    carrierBasePositionLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//                    TextView carrierBasePositionLabelTextView = new TextView(this);
//                    carrierBasePositionLabelTextView.setBackgroundColor(Color.parseColor("#EEEEEE"));
//                    carrierBasePositionLabelTextView.setText("Carrier Base Position");
//                    carrierBasePositionLabelTextView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1f));
//                    carrierBasePositionLabelTextView.setPadding(padding,padding,padding,padding);
//                    TextView carrierBasePositionValueTextView = new TextView(this);
//                    carrierBasePositionValueTextView.setBackgroundColor(Color.parseColor("#EEEEEE"));
//                    carrierBasePositionValueTextView.setText(carriersJsonArray.getJSONObject(i).getString("base_position"));
//                    carrierBasePositionValueTextView.setGravity(Gravity.RIGHT);
//                    carrierBasePositionValueTextView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1f));
//                    carrierBasePositionValueTextView.setTypeface(Typeface.DEFAULT_BOLD);
//                    carrierBasePositionValueTextView.setPadding(padding,padding,padding,padding);
//                    carrierBasePositionLinearLayout.addView(carrierBasePositionLabelTextView);
//                    carrierBasePositionLinearLayout.addView(carrierBasePositionValueTextView);
//                    carrierInfoLinearLayout.addView(carrierBasePositionLinearLayout);

//                    LinearLayout carrierPositionLinearLayout = new LinearLayout(this);
//                    carrierPositionLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//                    TextView carrierPositionLabelTextView = new TextView(this);
//                    carrierPositionLabelTextView.setBackgroundColor(Color.parseColor("#DDDDDD"));
//                    carrierPositionLabelTextView.setText("Carrier Position");
//                    carrierPositionLabelTextView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1f));
//                    carrierPositionLabelTextView.setPadding(padding,padding,padding,padding);
//                    TextView carrierPositionValueTextView = new TextView(this);
//                    carrierPositionValueTextView.setBackgroundColor(Color.parseColor("#DDDDDD"));
//                    carrierPositionValueTextView.setText(carriersJsonArray.getJSONObject(i).getString("position"));
//                    carrierPositionValueTextView.setGravity(Gravity.RIGHT);
//                    carrierPositionValueTextView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1f));
//                    carrierPositionValueTextView.setTypeface(Typeface.DEFAULT_BOLD);
//                    carrierPositionValueTextView.setPadding(padding,padding,padding,padding);
//                    carrierPositionLinearLayout.addView(carrierPositionLabelTextView);
//                    carrierPositionLinearLayout.addView(carrierPositionValueTextView);
//                    carrierInfoLinearLayout.addView(carrierPositionLinearLayout);

                    LinearLayout carrierStatusLinearLayout = new LinearLayout(this);
                    carrierStatusLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    TextView carrierStatusLabelTextView = new TextView(this);
                    carrierStatusLabelTextView.setBackgroundColor(Color.parseColor("#DDDDDD"));
                    carrierStatusLabelTextView.setText("Carrier Status");
                    carrierStatusLabelTextView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1f));
                    carrierStatusLabelTextView.setPadding(padding,padding,padding,padding);
                    TextView carrierStatusValueTextView = new TextView(this);
                    carrierStatusValueTextView.setBackgroundColor(Color.parseColor("#DDDDDD"));
                    carrierStatusValueTextView.setText(carriersJsonArray.getJSONObject(i).getString("status"));
                    carrierStatusValueTextView.setGravity(Gravity.RIGHT);
                    carrierStatusValueTextView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1f));
                    carrierStatusValueTextView.setTypeface(Typeface.DEFAULT_BOLD);
                    carrierStatusValueTextView.setPadding(padding,padding,padding,padding);
                    carrierStatusLinearLayout.addView(carrierStatusLabelTextView);
                    carrierStatusLinearLayout.addView(carrierStatusValueTextView);
                    carrierInfoLinearLayout.addView(carrierStatusLinearLayout);

//                    LinearLayout carrierCompanyIdLinearLayout = new LinearLayout(this);
//                    carrierCompanyIdLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//                    TextView carrierCompanyIdLabelTextView = new TextView(this);
//                    carrierCompanyIdLabelTextView.setBackgroundColor(Color.parseColor("#EEEEEE"));
//                    carrierCompanyIdLabelTextView.setText("Company ID");
//                    carrierCompanyIdLabelTextView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1f));
//                    carrierCompanyIdLabelTextView.setPadding(padding,padding,padding,padding);
//                    TextView carrierCompanyIdValueTextView = new TextView(this);
//                    carrierCompanyIdValueTextView.setBackgroundColor(Color.parseColor("#EEEEEE"));
//                    carrierCompanyIdValueTextView.setText(carriersJsonArray.getJSONObject(i).getString("company_id"));
//                    carrierCompanyIdValueTextView.setGravity(Gravity.RIGHT);
//                    carrierCompanyIdValueTextView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1f));
//                    carrierCompanyIdValueTextView.setTypeface(Typeface.DEFAULT_BOLD);
//                    carrierCompanyIdValueTextView.setPadding(padding,padding,padding,padding);
//                    carrierCompanyIdLinearLayout.addView(carrierCompanyIdLabelTextView);
//                    carrierCompanyIdLinearLayout.addView(carrierCompanyIdValueTextView);
//                    carrierInfoLinearLayout.addView(carrierCompanyIdLinearLayout);

                    LinearLayout carrierCompanyNameLinearLayout = new LinearLayout(this);
                    carrierCompanyNameLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    TextView carrierCompanyNameLabelTextView = new TextView(this);
                    carrierCompanyNameLabelTextView.setBackgroundColor(Color.parseColor("#EEEEEE"));
                    carrierCompanyNameLabelTextView.setText("Company Name");
                    carrierCompanyNameLabelTextView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1f));
                    carrierCompanyNameLabelTextView.setPadding(padding,padding,padding,padding);
                    TextView carrierCompanyNameValueTextView = new TextView(this);
                    carrierCompanyNameValueTextView.setBackgroundColor(Color.parseColor("#EEEEEE"));
                    JSONObject companyJsonObject = carriersJsonArray.getJSONObject(i).getJSONObject("company");
                    carrierCompanyNameValueTextView.setText(companyJsonObject.getString("name"));
                    carrierCompanyNameValueTextView.setGravity(Gravity.RIGHT);
                    carrierCompanyNameValueTextView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1f));
                    carrierCompanyNameValueTextView.setTypeface(Typeface.DEFAULT_BOLD);
                    carrierCompanyNameValueTextView.setPadding(padding,padding,padding,padding);
                    carrierCompanyNameLinearLayout.addView(carrierCompanyNameLabelTextView);
                    carrierCompanyNameLinearLayout.addView(carrierCompanyNameValueTextView);
                    carrierInfoLinearLayout.addView(carrierCompanyNameLinearLayout);

//                    LinearLayout carrierCompanyPositionLinearLayout = new LinearLayout(this);
//                    carrierCompanyPositionLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//                    TextView carrierCompanyPositionLabelTextView = new TextView(this);
//                    carrierCompanyPositionLabelTextView.setBackgroundColor(Color.parseColor("#EEEEEE"));
//                    carrierCompanyPositionLabelTextView.setText("Company Position");
//                    carrierCompanyPositionLabelTextView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1f));
//                    carrierCompanyPositionLabelTextView.setPadding(padding,padding,padding,padding);
//                    TextView carrierCompanyPositionValueTextView = new TextView(this);
//                    carrierCompanyPositionValueTextView.setBackgroundColor(Color.parseColor("#EEEEEE"));
//                    carrierCompanyPositionValueTextView.setText(companyJsonObject.getString("position"));
//                    carrierCompanyPositionValueTextView.setGravity(Gravity.RIGHT);
//                    carrierCompanyPositionValueTextView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1f));
//                    carrierCompanyPositionValueTextView.setTypeface(Typeface.DEFAULT_BOLD);
//                    carrierCompanyPositionValueTextView.setPadding(padding,padding,padding,padding);
//                    carrierCompanyPositionLinearLayout.addView(carrierCompanyPositionLabelTextView);
//                    carrierCompanyPositionLinearLayout.addView(carrierCompanyPositionValueTextView);
//                    carrierInfoLinearLayout.addView(carrierCompanyPositionLinearLayout);

                    LinearLayout carrierDateModifiedLinearLayout = new LinearLayout(this);
                    carrierDateModifiedLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    TextView carrierDateModifiedLabelTextView = new TextView(this);
                    carrierDateModifiedLabelTextView.setBackgroundColor(Color.parseColor("#DDDDDD"));
                    carrierDateModifiedLabelTextView.setText("Date Modified");
                    carrierDateModifiedLabelTextView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1f));
                    carrierDateModifiedLabelTextView.setPadding(padding,padding,padding,padding);
                    TextView carrierDateModifiedValueTextView = new TextView(this);
                    carrierDateModifiedValueTextView.setBackgroundColor(Color.parseColor("#DDDDDD"));
                    Calendar datemodified = Calendar.getInstance(TimeZone.getDefault());
                    datemodified.setTimeInMillis(Long.valueOf(carriersJsonArray.getJSONObject(i).getString("datemodified")) * 1000L);
                    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                    carrierDateModifiedValueTextView.setText(formatter.format(datemodified.getTime()));
                    carrierDateModifiedValueTextView.setGravity(Gravity.RIGHT);
                    carrierDateModifiedValueTextView.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1f));
                    carrierDateModifiedValueTextView.setTypeface(Typeface.DEFAULT_BOLD);
                    carrierDateModifiedValueTextView.setPadding(padding,padding,padding,padding);
                    carrierDateModifiedLinearLayout.addView(carrierDateModifiedLabelTextView);
                    carrierDateModifiedLinearLayout.addView(carrierDateModifiedValueTextView);
                    carrierInfoLinearLayout.addView(carrierDateModifiedLinearLayout);

                    carriersLinearLayout.addView(carrierInfoLinearLayout);

                    if (i < carriersJsonArray.length() - 1){
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(carrierInfoLinearLayout.getLayoutParams());
                        lp.setMargins(0, 0, 0, paddingBottom);
                        carrierInfoLinearLayout.setLayoutParams(lp);
                    }
                }
                mainTextView.setVisibility(View.GONE);
                mainWebView.setVisibility(View.GONE);
                mainTextLinearLayout.setVisibility(View.GONE);
            } else {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Status: ");
                stringBuilder.append(responseJsonObject.getString("status"));
                stringBuilder.append(System.getProperty("line.separator"));
                stringBuilder.append("Message: ");
                stringBuilder.append(responseJsonObject.getString("message"));
                mainTextView.setText(stringBuilder.toString().trim());
                mainTextView.setVisibility(View.GONE);
                mainWebView.loadData(stringBuilder.toString().trim(),"text/html", "UTF-8");
                mainWebView.setVisibility(View.VISIBLE);
                mainTextLinearLayout.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            mainTextView.setText(message);
            mainTextView.setVisibility(View.GONE);
            mainWebView.loadData(message,"text/html", "UTF-8");
            mainWebView.setVisibility(View.VISIBLE);
            mainTextLinearLayout.setVisibility(View.VISIBLE);
        }

        progress.dismiss();

        countDownTimer = new CountDownTimer(DELAY_INTERVAL, 1000) {

            public void onTick(long millisUntilFinished) {
                timerTextView.setText("Next update in: " + millisUntilFinished / 1000 + " seconds");
            }

            public void onFinish() {
                timerTextView.setText("Updating...");
                executeUpdate();
            }
        }.start();

        MyConnection myConnection = new MyConnection(MainActivity.this);
        myConnection.action = String.valueOf(ACTION_GET_MY_DISPATCHS);
        myConnection.requestURL = getMyDispatchsURL + "?imei=" + deviceId;
        myConnection.execute();
    }

    public void getMyDispatchsReturn(String response, String message) {
        try {
            JSONObject responseJsonObject = new JSONObject(response);
            JSONArray carriersJsonArray = responseJsonObject.getJSONArray("carriers");
            JSONObject userJsonObject;
            JSONObject addressJsonObject;
            int pendingRow = 1;
            int deliveredRow = 1;

            for (int i = 0; i < carriersJsonArray.length(); i++) {
                JSONArray dispatchsPendingJsonArray = carriersJsonArray.getJSONObject(i).getJSONArray("dispatchsPending");
                for (int k = 0; k < dispatchsPendingJsonArray.length(); k++) {
                    final TableRow tableRow = (TableRow) myPendingDispatchsTableLayout.getChildAt(pendingRow);
                    final TableRow tableRowAddress = (TableRow) myPendingDispatchsTableLayout.getChildAt(pendingRow + 1);
                    tableRowAddress.setVisibility(View.GONE);
                    pendingRow = pendingRow + 2;
                    myPendingDispatchsTableLayout.setVisibility(View.VISIBLE);
                    TextView myPendingDispatchIdTextView = (TextView) tableRow.getChildAt(0);
                    TextView myPendingDispatchUserTextView = (TextView) tableRow.getChildAt(1);
                    LinearLayout myPendingDispatchsDestinationLinearLayout = (LinearLayout) tableRow.getChildAt(2);
                    final ImageButton myPendingDispatchsDestinationImageButton = (ImageButton) myPendingDispatchsDestinationLinearLayout.getChildAt(0);
                    LinearLayout myPendingDispatchsLinearLayout = (LinearLayout) tableRow.getChildAt(3);
                    ImageButton myPendingDispatchsImageButton = (ImageButton) myPendingDispatchsLinearLayout.getChildAt(0);
                    TextView myPendingDispatchAddressTextView = (TextView) tableRowAddress.getChildAt(0);

                    myPendingDispatchIdTextView.setText(dispatchsPendingJsonArray.getJSONObject(k).getString("id"));
                    userJsonObject = dispatchsPendingJsonArray.getJSONObject(k).getJSONObject("user");
                    myPendingDispatchUserTextView.setText(userJsonObject.getString("name"));
                    addressJsonObject = dispatchsPendingJsonArray.getJSONObject(k).getJSONObject("address");
                    myPendingDispatchAddressTextView.setText(addressJsonObject.getString("address"));

                    final String deliveryToken = dispatchsPendingJsonArray.getJSONObject(k).getString("token");
                    myPendingDispatchsImageButton.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            new AlertDialog.Builder(MainActivity.this)
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setTitle("Delivered Dispatch")
                                    .setMessage("Are you sure you want to set this dispatch as delivered?")
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            progress.setMessage("Updating dispatchs");
                                            progress.show();
                                            MyConnection myConnection = new MyConnection(MainActivity.this);
                                            myConnection.action = String.valueOf(ACTION_UPDATE_DISPATCH);
                                            myConnection.requestURL = updateDispatchURL + "?deliveryToken=" + deliveryToken + "&status=Delivered";
                                            myConnection.execute();
                                        }
                                    })
                                    .setNegativeButton("No", null)
                                    .show();
                        }
                    });

                    myPendingDispatchsDestinationImageButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(tableRowAddress.getVisibility() == View.VISIBLE){
                                tableRowAddress.setVisibility(View.GONE);
                                myPendingDispatchsDestinationImageButton.setImageResource(R.mipmap.down);
                            }else{
                                tableRowAddress.setVisibility(View.VISIBLE);
                                myPendingDispatchsDestinationImageButton.setImageResource(R.mipmap.up);
                            }
                        }
                    });
                }

                JSONArray dispatchsDeliveredJsonArray = carriersJsonArray.getJSONObject(i).getJSONArray("dispatchsDelivered");
                for (int k = 0; k < dispatchsDeliveredJsonArray.length(); k++) {
                    TableRow tableRow = (TableRow) myDeliveredDispatchsTableLayout.getChildAt(deliveredRow);
                    final TableRow tableRowAddress = (TableRow) myDeliveredDispatchsTableLayout.getChildAt(deliveredRow + 1);
                    tableRowAddress.setVisibility(View.GONE);
                    deliveredRow = deliveredRow + 2;
                    tableRow.setVisibility(View.VISIBLE);
                    myDeliveredDispatchsTableLayout.setVisibility(View.VISIBLE);
                    TextView myDeliveredDispatchIdTextView = (TextView) tableRow.getChildAt(0);
                    TextView myDeliveredDispatchUserTextView = (TextView) tableRow.getChildAt(1);
                    TextView myDeliveredDispatchTimeEndTextView = (TextView) tableRow.getChildAt(2);
                    LinearLayout myDeliveredDispatchsDestinationLinearLayout = (LinearLayout) tableRow.getChildAt(3);
                    final ImageButton myDeliveredDispatchsDestinationImageButton = (ImageButton) myDeliveredDispatchsDestinationLinearLayout.getChildAt(0);;

                    myDeliveredDispatchIdTextView.setText(dispatchsDeliveredJsonArray.getJSONObject(k).getString("id"));
                    userJsonObject = dispatchsDeliveredJsonArray.getJSONObject(k).getJSONObject("user");
                    myDeliveredDispatchUserTextView.setText(userJsonObject.getString("name"));

                    try {
                        SimpleDateFormat formatParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                        Date formatedDate = formatParser.parse(dispatchsDeliveredJsonArray.getJSONObject(k).getString("timeend"));
                        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                        myDeliveredDispatchTimeEndTextView.setText(formatter.format(formatedDate));
                    } catch (ParseException e) {
                        myDeliveredDispatchTimeEndTextView.setText(dispatchsDeliveredJsonArray.getJSONObject(k).getString("timeend"));
                    }

                    TextView myDeliveredDispatchAddressTextView = (TextView) tableRowAddress.getChildAt(0);
                    addressJsonObject = dispatchsDeliveredJsonArray.getJSONObject(k).getJSONObject("address");
                    myDeliveredDispatchAddressTextView.setText(addressJsonObject.getString("address"));

                    myDeliveredDispatchsDestinationImageButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(tableRowAddress.getVisibility() == View.VISIBLE){
                                tableRowAddress.setVisibility(View.GONE);
                                myDeliveredDispatchsDestinationImageButton.setImageResource(R.mipmap.down);
                            }else{
                                tableRowAddress.setVisibility(View.VISIBLE);
                                myDeliveredDispatchsDestinationImageButton.setImageResource(R.mipmap.up);
                            }
                        }
                    });
                }
            }

            if (pendingRow == 1) {
                myPendingDispatchsTableLayout.setVisibility(View.GONE);
                noPendingDispatchsTextView.setVisibility(View.VISIBLE);
            } else {
                myPendingDispatchsTableLayout.setVisibility(View.VISIBLE);
                noPendingDispatchsTextView.setVisibility(View.GONE);

                for (int k = pendingRow; k <= 40; k++) {
                    TableRow tableRow = (TableRow) myPendingDispatchsTableLayout.getChildAt(k);
                    tableRow.setVisibility(View.GONE);
                }
            }

            if (deliveredRow == 1) {
                myDeliveredDispatchsTableLayout.setVisibility(View.GONE);
                noDeliveredDispatchsTextView.setVisibility(View.VISIBLE);
            } else {
                myDeliveredDispatchsTableLayout.setVisibility(View.VISIBLE);
                noDeliveredDispatchsTextView.setVisibility(View.GONE);

                for (int k = deliveredRow; k <= 40; k++) {
                    TableRow tableRow = (TableRow) myDeliveredDispatchsTableLayout.getChildAt(k);
                    tableRow.setVisibility(View.GONE);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        progress.dismiss();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    private class MyConnection extends AsyncTask<String, Void, String> {

        private final Context context;
        private String response;
        HttpURLConnection urlConnection;
        String action;
        String requestURL;

        public MyConnection(Context c) {
            this.context = c;
        }

        @Override
        protected void onPreExecute() {
            preExecute(action);
        }

        @Override
        protected String doInBackground(String[] params) {
            try {
                response = performGetCall(requestURL);

//                response = "{\n" +
//                        "status: \"OK\",\n" +
//                        "carriers: [\n" +
//                        "{\n" +
//                        "id: 1,\n" +
//                        "name: \"Entregador 1\",\n" +
//                        "base_position: \"\",\n" +
//                        "position: \"{lat: -27.589072, lng: -48.501146}\",\n" +
//                        "status: \"Online\",\n" +
//                        "datemodified: 1493063690,\n" +
//                        "company_id: 1,\n" +
//                        "imei: \"000000000000000\",\n" +
//                        "resource_id: 3,\n" +
//                        "company: {\n" +
//                        "id: 1,\n" +
//                        "name: \"Empresa 1\",\n" +
//                        "position: \"{lat: -27.590114, lng: -48.543081}\",\n" +
//                        "resource_id: 1\n" +
//                        "},\n" +
//                        "resource: {\n" +
//                        "id: 3,\n" +
//                        "resource: \"Carrier\",\n" +
//                        "type: \"Free\",\n" +
//                        "plan_id: 1\n" +
//                        "}\n" +
//                        "},\n" +
//                        "{\n" +
//                        "id: 2,\n" +
//                        "name: \"Entregador 2\",\n" +
//                        "base_position: \"\",\n" +
//                        "position: \"{lat: -27.589072, lng: -48.501146}\",\n" +
//                        "status: \"Online\",\n" +
//                        "datemodified: 1493063690,\n" +
//                        "company_id: 1,\n" +
//                        "imei: \"000000000000000\",\n" +
//                        "resource_id: 4,\n" +
//                        "company: {\n" +
//                        "id: 1,\n" +
//                        "name: \"Empresa 1\",\n" +
//                        "position: \"{lat: -27.590114, lng: -48.543081}\",\n" +
//                        "resource_id: 1\n" +
//                        "},\n" +
//                        "resource: {\n" +
//                        "id: 4,\n" +
//                        "resource: \"Carrier\",\n" +
//                        "type: \"Free\",\n" +
//                        "plan_id: 1\n" +
//                        "}\n" +
//                        "},\n" +
//                        "{\n" +
//                        "id: 4,\n" +
//                        "name: \"Entregador 4\",\n" +
//                        "base_position: \"\",\n" +
//                        "position: \"{lat: -27.589072, lng: -48.501146}\",\n" +
//                        "status: \"Online\",\n" +
//                        "datemodified: 1493063691,\n" +
//                        "company_id: 5,\n" +
//                        "imei: \"000000000000000\",\n" +
//                        "resource_id: 10,\n" +
//                        "company: {\n" +
//                        "id: 5,\n" +
//                        "name: \"Empresa 4\",\n" +
//                        "position: \"{lat: -27.587187, lng: -48.498172}\",\n" +
//                        "resource_id: 9\n" +
//                        "},\n" +
//                        "resource: {\n" +
//                        "id: 10,\n" +
//                        "resource: \"Carrier\",\n" +
//                        "type: \"Free\",\n" +
//                        "plan_id: 4\n" +
//                        "}\n" +
//                        "}\n" +
//                        "]\n" +
//                        "}";

//                response = "{\n" +
//                        "carriers: [\n" +
//                        "{\n" +
//                        "id: 1,\n" +
//                        "name: \"Entregador 1\",\n" +
//                        "base_position: \"\",\n" +
//                        "position: \"{lat: -27.589072, lng: -48.501146}\",\n" +
//                        "status: \"Online\",\n" +
//                        "datemodified: \"2017-04-11T22:17:07-03:00\",\n" +
//                        "company_id: 1,\n" +
//                        "imei: \"000000000000000\",\n" +
//                        "resource_id: 3,\n" +
//                        "dispatchsPending: [\n" +
//                        "{\n" +
//                        "id: 1,\n" +
//                        "user_id: 2,\n" +
//                        "destination: \"{lat: -27.592712, lng: -48.521353}\",\n" +
//                        "carrier_id: 1,\n" +
//                        "timestart: \"2016-11-11T15:10:00-02:00\",\n" +
//                        "timeend: null,\n" +
//                        "status: \"Pending\",\n" +
//                        "user: {\n" +
//                        "id: 2,\n" +
//                        "name: \"Usuário\",\n" +
//                        "username: \"user\",\n" +
//                        "email: \"lpdemilis@gmail.com\",\n" +
//                        "created: \"2016-11-11T15:02:59-02:00\",\n" +
//                        "modified: \"2016-11-16T10:52:03-02:00\"\n" +
//                        "}\n" +
//                        "},\n" +
//                        "{\n" +
//                        "id: 4,\n" +
//                        "user_id: 1,\n" +
//                        "destination: \"{lat: -27.595365, lng: -48.517555}\",\n" +
//                        "carrier_id: 1,\n" +
//                        "timestart: \"2016-11-11T15:14:00-02:00\",\n" +
//                        "timeend: null,\n" +
//                        "status: \"Pending\",\n" +
//                        "user: {\n" +
//                        "id: 1,\n" +
//                        "name: \"Administrador\",\n" +
//                        "username: \"admin\",\n" +
//                        "email: \"lpdemilis@gmail.com\",\n" +
//                        "created: \"2016-11-11T15:01:43-02:00\",\n" +
//                        "modified: \"2016-11-11T15:01:43-02:00\"\n" +
//                        "}\n" +
//                        "}\n" +
//                        "],\n" +
//                        "dispatchsDelivered: [ ]\n" +
//                        "},\n" +
//                        "{\n" +
//                        "id: 2,\n" +
//                        "name: \"Entregador 2\",\n" +
//                        "base_position: \"\",\n" +
//                        "position: \"{lat: -27.589072, lng: -48.501146}\",\n" +
//                        "status: \"Online\",\n" +
//                        "datemodified: \"2017-04-11T22:17:08-03:00\",\n" +
//                        "company_id: 1,\n" +
//                        "imei: \"000000000000000\",\n" +
//                        "resource_id: 4,\n" +
//                        "dispatchsPending: [\n" +
//                        "{\n" +
//                        "id: 2,\n" +
//                        "user_id: 1,\n" +
//                        "destination: \"{lat: -27.595365, lng: -48.517555}\",\n" +
//                        "carrier_id: 2,\n" +
//                        "timestart: \"2016-11-11T15:14:00-02:00\",\n" +
//                        "timeend: null,\n" +
//                        "status: \"Pending\",\n" +
//                        "user: {\n" +
//                        "id: 1,\n" +
//                        "name: \"Administrador\",\n" +
//                        "username: \"admin\",\n" +
//                        "email: \"lpdemilis@gmail.com\",\n" +
//                        "created: \"2016-11-11T15:01:43-02:00\",\n" +
//                        "modified: \"2016-11-11T15:01:43-02:00\"\n" +
//                        "}\n" +
//                        "}\n" +
//                        "],\n" +
//                        "dispatchsDelivered: [ ]\n" +
//                        "},\n" +
//                        "{\n" +
//                        "id: 4,\n" +
//                        "name: \"Entregador 4\",\n" +
//                        "base_position: \"\",\n" +
//                        "position: \"{lat: -27.589072, lng: -48.501146}\",\n" +
//                        "status: \"Offline\",\n" +
//                        "datemodified: \"2017-04-12T21:29:32-03:00\",\n" +
//                        "company_id: 5,\n" +
//                        "imei: \"000000000000000\",\n" +
//                        "resource_id: 10,\n" +
//                        "dispatchsPending: [\n" +
//                        "{\n" +
//                        "id: 6,\n" +
//                        "user_id: 1,\n" +
//                        "destination: \"{lat:-26.3044084, lng: -48.84638319999999}\",\n" +
//                        "carrier_id: 4,\n" +
//                        "timestart: \"2016-11-28T16:57:00-02:00\",\n" +
//                        "timeend: null,\n" +
//                        "status: \"Pending\",\n" +
//                        "user: {\n" +
//                        "id: 1,\n" +
//                        "name: \"Administrador\",\n" +
//                        "username: \"admin\",\n" +
//                        "email: \"lpdemilis@gmail.com\",\n" +
//                        "created: \"2016-11-11T15:01:43-02:00\",\n" +
//                        "modified: \"2016-11-11T15:01:43-02:00\"\n" +
//                        "}\n" +
//                        "},\n" +
//                        "{\n" +
//                        "id: 7,\n" +
//                        "user_id: 2,\n" +
//                        "destination: \"{lat:-27.5918875, lng: -48.493988599999966}\",\n" +
//                        "carrier_id: 4,\n" +
//                        "timestart: \"2016-12-21T19:30:00-02:00\",\n" +
//                        "timeend: null,\n" +
//                        "status: \"Pending\",\n" +
//                        "user: {\n" +
//                        "id: 2,\n" +
//                        "name: \"Usuário\",\n" +
//                        "username: \"user\",\n" +
//                        "email: \"lpdemilis@gmail.com\",\n" +
//                        "created: \"2016-11-11T15:02:59-02:00\",\n" +
//                        "modified: \"2016-11-16T10:52:03-02:00\"\n" +
//                        "}\n" +
//                        "}\n" +
//                        "],\n" +
//                        "dispatchsDelivered: [\n" +
//                        "{\n" +
//                        "id: 5,\n" +
//                        "user_id: 2,\n" +
//                        "destination: \"{lat:-27.5930711, lng: -48.55756550000001}\",\n" +
//                        "carrier_id: 4,\n" +
//                        "timestart: \"2016-11-28T13:34:00-02:00\",\n" +
//                        "timeend: \"2016-11-28T19:53:00-02:00\",\n" +
//                        "status: \"Delivered\",\n" +
//                        "user: {\n" +
//                        "id: 2,\n" +
//                        "name: \"Usuário\",\n" +
//                        "username: \"user\",\n" +
//                        "email: \"lpdemilis@gmail.com\",\n" +
//                        "created: \"2016-11-11T15:02:59-02:00\",\n" +
//                        "modified: \"2016-11-16T10:52:03-02:00\"\n" +
//                        "}\n" +
//                        "}\n" +
//                        "]\n" +
//                        "}\n" +
//                        "]\n" +
//                        "}";

                return response;

            } catch (Exception e) {
                e.printStackTrace();
                response = String.valueOf(HttpsURLConnection.HTTP_BAD_REQUEST);
                return null;
            }
        }

        @Override
        protected void onPostExecute(String message) {
            postExecute(response, action, message);
        }

        public String performGetCall(String requestURL) {
            StringBuilder result = new StringBuilder();

            try {
                URL url = new URL(requestURL);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
            } catch (Exception e) {
                e.printStackTrace();
                result = new StringBuilder();
                result.append(e.getCause());
            } finally {
                urlConnection.disconnect();
            }

            return result.toString();
        }
    }
}